using NUnit.Framework;
using System.Threading.Tasks;
using WordLocker.Business.Logic;

namespace WordLocker.Tests;

public class CryptographyTests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public async Task Decrypt_String()
    {
        // Arrange
        string privateKeyPath = "../../../Keys/key.gpg";
        string privateKeyPassword = "password";
        string encryptedData = @"-----BEGIN PGP MESSAGE-----

hQGMA8LvJQmN5rPQAQwAlwCSlEcbhzoMzCboxtuyqbZ4sVOYLz0iC0MEJ+1QeVWf
QcPlu3lEGQUd/2leCrW+reFu7zaWkG+QzTLQ8EaWSPCjtfKX+GodLzg4NN2oqo2w
+Z6Qu55KpOM9ys6t4K2xwkSglaIBvk5zVCgj6wIQRiUx8qToI664qHZPGqIz/eMn
2f5EvZX8d/0h5EYpu/m6aYBpdKS4MHPlvqNnC7uiSEqQF3e/7oGtdFN9fA375HBi
ZtmowqXdXGlpCgurc4YT7G6yhSLNgQc59G1BPI2y+69fD2Fqy6LaPkbqyRQxTDlW
UKorwJ6Y02p7Y3sVDVQgNeTDGcR6+OmE1N00toB3jSVwbFQ/Vi3esGUy1ios20EP
+Nt0rVObmtgsQeNuAMcA7OJrktUomYzstTMuRn2PrfOqytAI5cQCS9U/JJMAZyiR
bJPhWz2mRVWrQSkbSTjV5fd8wDK4TB6c0T1VLIJ3V6cjb8TDO3GnOxjHA+/Ksdu8
RDV7pNUlcw29X0STnDOv1FUBCQIQHQtws3bwV92b/OMKdTKzD4d69zdxI6Fi1Lwt
4H78YWgDxBE5HpKlw67mdiZ4yTwWtkDCGLJIkyIwIUAPpNVl5a408gEbaZJyqnSP
juu6q8Qm
=DPtH
-----END PGP MESSAGE-----
";

        Cryptography cryptography = new Cryptography();

        // Act
        string decryptedData = await cryptography.Decrypt(privateKeyPath, privateKeyPassword, encryptedData);

        // Assert
        Assert.That(decryptedData.Equals("data to encrypt"));
        // Add more assertions as needed
    }

    [Test]
    public async Task Encrypt_String()
    {
        // Arrange
        string publicKeyPath = "../../../Keys/key.gpg.pub";
        string data = "data to encrypt";
    
        Cryptography cryptography = new Cryptography();
    
        // Act
        string encryptedData = await cryptography.Encrypt(publicKeyPath, data);
    
        // Assert
        Assert.That(encryptedData != null);
        // Add more assertions as needed
    }
}