using System.ComponentModel;

namespace WordLocker.Presentation;

public partial class MainViewModel : ObservableObject
{
    private INavigator _navigator;

    public string? Title { get; }

    [ObservableProperty]
    private Password? selectedPassword;

    [ObservableProperty]
    private DecryptableCollection<Password>? passwords;

    [ObservableProperty]
    private string? name;

    public ICommand GoToSettings { get; }

    #pragma warning disable CS8618 //Password is initialized in InitializePasswords
    public MainViewModel(INavigator navigator)
    {
        _navigator = navigator;
        Title = "Main";
        InitializePasswords();

        Configuration.Instance.PropertyChanged += Configuration_PropertyChanged;

        GoToSettings = new RelayCommand(OpenSettings);
    }
    #pragma warning restore CS8618

    private void InitializePasswords()
    {
        if (Configuration.Instance.FilterByExtension == true)
        {
            Passwords = new DecryptableCollection<Password>(Configuration.Instance.RepositoryPath, Configuration.Instance.FileExtension);
        }
        else
        {
            Passwords = new DecryptableCollection<Password>(Configuration.Instance.RepositoryPath);
        }
    }

    private async void OpenSettings()
    {
        await _navigator.NavigateViewModelAsync<SettingsViewModel>(this, data: new Entity(Name!));
    }

    private void Configuration_PropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        InitializePasswords();
    }

    partial void OnSelectedPasswordChanged(Password? value)
    {
    }
}
