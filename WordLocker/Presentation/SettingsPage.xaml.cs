﻿namespace WordLocker.Presentation;

public sealed partial class SettingsPage : Page
{
    public SettingsPage()
    {
        this.InitializeComponent();
    }

    private void ToggleFileExtensionTextBox(object sender, RoutedEventArgs e)
    {
        if (FilterByExtensionCheckBox.IsChecked != false)
        {
            FileExtensionTextBox.IsEnabled = true;
        }
        else
        {
            FileExtensionTextBox.IsEnabled = false;
        }
    }
}

