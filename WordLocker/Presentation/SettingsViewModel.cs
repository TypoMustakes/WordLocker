using WordLocker.Business.Logic;
using Windows.Storage.Pickers;

namespace WordLocker.Presentation;

public partial class SettingsViewModel : ObservableObject
{
    private INavigator _navigator;

    [ObservableProperty]
    private string? name;
    public string? Title { get; }

    public SettingsViewModel(
        INavigator navigator)
    {
        _navigator = navigator;
        Title = "Settings";

        SaveSettingsCommand = new RelayCommand(SaveSettings);
        BrowsePrivateKeyCommand = new RelayCommand(BrowsePrivateKey);
        BrowsePublicKeyCommand = new RelayCommand(BrowsePublicKey);
        BrowseRepositoryCommand = new RelayCommand(BrowseRepository);
        GenerateKeyPairCommand = new RelayCommand(GenerateKeyPair);
        CancelCommand = new RelayCommand(Cancel);

        PublicKeyPath = Configuration.Instance.PublicKeyPath;
        PrivateKeyPath = Configuration.Instance.PrivateKeyPath;
        RepositoryPath = Configuration.Instance.RepositoryPath;
        FilterByExtension = Configuration.Instance.FilterByExtension;
        FileExtension = Configuration.Instance.FileExtension;
    }

    [ObservableProperty]
    private string? publicKeyPath;

    [ObservableProperty]
    private string? privateKeyPath;

    [ObservableProperty]
    private string? repositoryPath;

    [ObservableProperty]
    private bool? filterByExtension;

    [ObservableProperty]
    private string? fileExtension;

    public ICommand SaveSettingsCommand { get; }
    public ICommand BrowsePrivateKeyCommand { get; }
    public ICommand BrowsePublicKeyCommand { get; }
    public ICommand BrowseRepositoryCommand { get; }
    public ICommand GenerateKeyPairCommand { get; }
    public ICommand CancelCommand { get; }

    private void SaveSettings()
    {
        ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.WithFileExtension(this.FileExtension)
            .WithFilterByExtension(this.FilterByExtension)
            .WithPrivateKeyPath(this.PrivateKeyPath)
            .WithPublicKeyPath(this.PublicKeyPath)
            .WithRepositoryPath(this.RepositoryPath)
            .SaveChanges();
    }

    private async void BrowsePrivateKey()
    {
        FileOpenPicker filePicker = new FileOpenPicker();
        filePicker.FileTypeFilter.Add("*");
        var storageFile = await filePicker.PickSingleFileAsync();

        if (storageFile != null)
        {
            PrivateKeyPath = storageFile.Path;
        }
        else
        {
            // Did not pick any file.
        }
    }

    private async void BrowsePublicKey()
    {
        FileOpenPicker filePicker = new FileOpenPicker();
        filePicker.FileTypeFilter.Add("*");
        var storageFile = await filePicker.PickSingleFileAsync();

        if (storageFile != null)
        {
            PublicKeyPath = storageFile.Path;
        }
        else
        {
            // Did not pick any file.
        }
    }

    // #if HAS_UNO
    private async void BrowseRepository()
    {
    	FolderPicker folderPicker = new FolderPicker();
    	folderPicker.FileTypeFilter.Add("*");
    
    	var storageFolder = await folderPicker.PickSingleFolderAsync();
    	if (storageFolder != null)
    	{
    		// var fileList = await storageFolder.GetFilesAsync();
    		// var folderList = await storageFolder.GetFoldersAsync();

            RepositoryPath = storageFolder.Path;
    	}
    	else
    	{
    		// Did not pick any folder.
    	}
    }
    // #endif

    private async void GenerateKeyPair()
    {
        await _navigator.NavigateViewModelAsync<GenerateKeyPairViewModel>(this, data: new Entity(Name!));
    }

    public async void Cancel()
    {
        if (await _navigator.CanGoBack())
        {
            await _navigator.GoBack(this);
        }
    }
}
