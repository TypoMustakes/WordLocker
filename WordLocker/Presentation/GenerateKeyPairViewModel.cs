﻿using Windows.Storage.Pickers;
using WordLocker.Business.Logic;

namespace WordLocker.Presentation;

public partial class GenerateKeyPairViewModel : ObservableObject
{
    private INavigator _navigator;

    [ObservableProperty]
    private string? name;
    public string? Title { get; }

    public GenerateKeyPairViewModel(
        INavigator navigator)
    {
        _navigator = navigator;
        Title = "Generate keypair";

        CancelCommand = new RelayCommand(Cancel);
        BrowsePrivateKeyCommand = new RelayCommand(BrowsePrivateKey);
        BrowsePublicKeyCommand = new RelayCommand(BrowsePublicKey);
        CreateCommand = new RelayCommand(Create);
    }

    [ObservableProperty]
    private string? publicKeyPath;

    [ObservableProperty]
    private string? privateKeyPath;

    [ObservableProperty]
    private string? keyID;

    [ObservableProperty]
    private string? password;

    public ICommand CancelCommand { get; }
    public ICommand BrowsePrivateKeyCommand { get; }
    public ICommand BrowsePublicKeyCommand { get; }
    public ICommand CreateCommand { get; }

    public async void Cancel()
    {
        if (await _navigator.CanGoBack())
        {
            await _navigator.GoBack(this);
        }
    }

    private async void BrowsePrivateKey()
    {
        FileSavePicker filePicker = new FileSavePicker();
        filePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;

        if (KeyID == null || KeyID == "")
        {
            filePicker.SuggestedFileName = "wordlocker_key.gpg";
        }
        else
        {
            filePicker.SuggestedFileName = KeyID + ".gpg";
        }

        var storageFile = await filePicker.PickSaveFileAsync();

        if (storageFile != null)
        {
            PrivateKeyPath = storageFile.Path;
        }
        else
        {
            // Did not pick any file.
        }
    }

    private async void BrowsePublicKey()
    {
        FileSavePicker filePicker = new FileSavePicker();
        filePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;

        if (KeyID == null || KeyID == "")
        {
            filePicker.SuggestedFileName = "wordlocker_key.gpg.pub";
        }
        else
        {
            filePicker.SuggestedFileName = KeyID + ".gpg.pub";
        }

        var storageFile = await filePicker.PickSaveFileAsync();

        if (storageFile != null)
        {
            PublicKeyPath = storageFile.Path;
        }
        else
        {
            // Did not pick any file.
        }
    }

    public void Create()
    {
        //write the keypair to file
        ConfigurationBuilder builder = new ConfigurationBuilder();
        builder.WithPublicKeyPath(PublicKeyPath)
            .WithPrivateKeyPath(PrivateKeyPath)
            .WithRepositoryPath(Configuration.Instance.RepositoryPath)
            .WithFilterByExtension(Configuration.Instance.FilterByExtension)
            .WithFileExtension(Configuration.Instance.FileExtension)
            .SaveChanges();
    }
}
