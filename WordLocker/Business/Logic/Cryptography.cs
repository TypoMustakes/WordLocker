﻿using PgpCore;
using System.IO;
namespace WordLocker.Business.Logic;

/// <summary>
/// Cryptography class to handle encryption and decryption of data using the PgpCore library.
/// This class is a singleton. Use the 'Instance' property to get the instance of this class.
/// </summary>
public class Cryptography
{
    public async Task<string> Encrypt(string publicKeyPath, string data)
    {
        // Load keys
        string publicKey = System.IO.File.ReadAllText(publicKeyPath);
        EncryptionKeys encryptionKeys = new EncryptionKeys(publicKey);

        // Encrypt
        PGP pgp = new PGP(encryptionKeys);
        return await pgp.EncryptAsync(data);
    }

    public async Task<string> Decrypt(string privateKeyPath, string privateKeyPassword, string data)
    {
        // Load keys
        string privateKey = System.IO.File.ReadAllText(privateKeyPath);
        EncryptionKeys encryptionKeys = new EncryptionKeys(privateKey, privateKeyPassword);

        // Decrypt
        PGP pgp = new PGP(encryptionKeys);
        return await pgp.DecryptAsync(data);
    }
}