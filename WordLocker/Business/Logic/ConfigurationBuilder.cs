﻿namespace WordLocker.Business.Logic;

public class ConfigurationBuilder
{
    private string? publicKeyPath;
    private string? privateKeyPath;
    private string? repositoryPath;
    private bool? filterByExtension;
    private string? fileExtension;

    public ConfigurationBuilder WithPublicKeyPath(string? publicKeyPath)
    {
        this.publicKeyPath = publicKeyPath;
        return this;
    }

    public ConfigurationBuilder WithPrivateKeyPath(string? privateKeyPath)
    {
        this.privateKeyPath = privateKeyPath;
        return this;
    }

    public ConfigurationBuilder WithRepositoryPath(string? repositoryPath)
    {
        this.repositoryPath = repositoryPath;
        return this;
    }

    public ConfigurationBuilder WithFilterByExtension(bool? filterByExtension)
    {
        this.filterByExtension = filterByExtension;
        return this;
    }

    public ConfigurationBuilder WithFileExtension(string? fileExtension)
    {
        this.fileExtension = fileExtension;
        return this;
    }

    public void SaveChanges()
    {
        var configuration = Configuration.Instance;
        configuration.PublicKeyPath = this.publicKeyPath;
        configuration.PrivateKeyPath = this.privateKeyPath;
        configuration.RepositoryPath = this.repositoryPath;
        configuration.FilterByExtension = this.filterByExtension;
        configuration.FileExtension = this.fileExtension;
    }
}
