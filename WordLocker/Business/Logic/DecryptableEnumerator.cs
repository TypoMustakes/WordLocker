﻿using System.IO;
using System.Text.Json;

namespace WordLocker.Business.Logic;

public class DecryptableEnumerator
{
    public Decryptable[] EnumerateFiles(string folderPath, string? filter = null)
    {
        string[] filePaths = Directory.GetFiles(folderPath);
        List<Decryptable> files = new List<Decryptable>();

        for (int i = 0; i < filePaths.Length; i++)
        {
            if (filter == null)
            {
                files.Add(new Decryptable(filePaths[i]));
            }
            else if (filePaths[i].EndsWith(filter))
            {
                files.Add(new Decryptable(filePaths[i]));
            }
        }

        return files.ToArray();
    }
}