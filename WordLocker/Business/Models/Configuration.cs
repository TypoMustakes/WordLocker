﻿using System.ComponentModel;

namespace WordLocker.Business.Models
{
    /// <summary>
    /// This class is a singleton. Use the 'Instance' property to get the instance of this class.
    /// Stores and provides access to the configuration settings of the application.
    /// </summary>
    public class Configuration
    {
        private static readonly object lockObject = new object();
        private static Configuration? instance;

        private string? publicKeyPath;
        private string? privateKeyPath;
        private string? repositoryPath;
        private bool? filterByExtension;
        private string? fileExtension;

        public string? PublicKeyPath
        {
            get { return publicKeyPath; }
            set
            {
                if (publicKeyPath != value)
                {
                    publicKeyPath = value;
                    OnPropertyChanged(nameof(PublicKeyPath));
                }
            }
        }

        public string? PrivateKeyPath
        {
            get { return privateKeyPath; }
            set
            {
                if (privateKeyPath != value)
                {
                    privateKeyPath = value;
                    OnPropertyChanged(nameof(PrivateKeyPath));
                }
            }
        }

        public string? RepositoryPath
        {
            get { return repositoryPath; }
            set
            {
                if (repositoryPath != value)
                {
                    repositoryPath = value;
                    OnPropertyChanged(nameof(RepositoryPath));
                }
            }
        }

        public bool? FilterByExtension
        {
            get { return filterByExtension; }
            set
            {
                if (filterByExtension != value)
                {
                    filterByExtension = value;
                    OnPropertyChanged(nameof(FilterByExtension));
                }
            }
        }

        public string? FileExtension
        {
            get { return fileExtension; }
            set
            {
                if (fileExtension != value)
                {
                    fileExtension = value;
                    OnPropertyChanged(nameof(FileExtension));
                }
            }
        }

        public static Configuration Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObject)
                    {
                        if (instance == null)
                        {
                            instance = new Configuration();
                        }
                    }
                }
                return instance;
            }
        }

        private Configuration()
        {
            this.FilterByExtension = true;
        }

        public event PropertyChangedEventHandler? PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
