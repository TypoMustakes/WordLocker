﻿using System.Text;

namespace WordLocker;

public class Password : Decryptable
{
    public string Name
    {
        get
        {
            string fileName = Path.GetFileNameWithoutExtension(FilePath);
            StringBuilder sb = new StringBuilder();
            foreach (char c in fileName)
            {
                if (Char.IsLetterOrDigit(c))
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }

    public Password(string path) : base(path) { }
    public Password() : base(String.Empty) { }
}
