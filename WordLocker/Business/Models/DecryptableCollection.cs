﻿using System.Collections;
using System.Collections.Generic;
using WordLocker.Business.Logic;

namespace WordLocker.Business.Models;

public class DecryptableCollection<T> : IEnumerable where T : Decryptable, new()
{
    private List<T> collection;

    public DecryptableCollection(string folderPath, string? filter = null)
    {
        collection = new List<T>();
        if (folderPath != null)
        {
            foreach (var file in new DecryptableEnumerator().EnumerateFiles(folderPath, filter))
            {
                var tmp = (Decryptable)new T();
                file.ConvertToDecryptable(ref tmp);
                collection.Add((T)tmp);
            }
        }
    }

    public T[] ToArray()
    {
        return collection.ToArray();
    }

    public void Add(T item)
    {
        collection.Add(item);
    }

    public void Remove(T item)
    {
        collection.Remove(item);
    }

    public void Clear()
    {
        collection.Clear();
    }

    public T Get(int index)
    {
        return collection[index];
    }

    public IEnumerator GetEnumerator()
    {
        return collection.GetEnumerator();
    }

    public T this[int index]
    {
        get => collection[index];
        set => collection[index] = value;
    }
}
