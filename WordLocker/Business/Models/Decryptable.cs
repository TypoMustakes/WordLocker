﻿namespace WordLocker.Business.Models;

public class Decryptable
{
    public string FilePath { get; set; }
    public Decryptable(string path)
    {
        FilePath = path;
    }

    public void ConvertToDecryptable(ref Decryptable instance)
    {
        instance.FilePath = this.FilePath;
    }
}
