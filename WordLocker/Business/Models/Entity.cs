namespace WordLocker.Business.Models;

public record Entity(string Name);
